import numpy as np
import matplotlib.pyplot as plt
from numba import jit,prange

@jit(nopython=True,fastmath=True)
def linear_interp(var1,dx1,var2,dx2):
    return ((dx2/(dx1+dx2))*var1)+((dx1/(dx1+dx2))*var2)


@jit(nopython=True,fastmath=True)
def compute_state_der_cfd(Pressure,u_vel,v_vel,dx,dy,beta,rho,nu,timestep):
    der_p=np.zeros_like(Pressure)
    der_u=np.zeros_like(u_vel)
    der_v=np.zeros_like(v_vel)
    # writing the derivatives using the continuty and the momentum equations 
    for i in range(2,len(dx)-2):
        for j in range(2,len(dy)-2):
            # Writing the variables to be used for writing the equations
            # Capital denotes the centre velocities
            dx_c=dx[i]
            dx_e=dx[i+1]
            dx_ee=dx[i+2]
            dx_w=dx[i-1]
            dx_ww=dx[i-2]
            
            dy_c=dy[j]
            dy_n=dy[j+1]
            dy_nn=dy[j+2]
            dy_s=dy[j-1]
            dy_ss=dy[j-2]
            
            u_C=u_vel[i,j]
            u_E=u_vel[i+1,j]
            u_W=u_vel[i-1,j]
            u_N=u_vel[i,j+1]
            u_S=u_vel[i,j-1]
            
            v_C=v_vel[i,j]
            v_E=v_vel[i+1,j]
            v_W=v_vel[i-1,j]
            v_N=v_vel[i,j+1]
            v_S=v_vel[i,j-1]
            
            p_C=Pressure[i,j]
            p_E=Pressure[i+1,j]
            p_EE=Pressure[i+2,j]
            p_W=Pressure[i-1,j]
            p_WW=Pressure[i-2,j]
            p_N=Pressure[i,j+1]
            p_NN=Pressure[i,j+2]
            p_S=Pressure[i,j-1]
            p_SS=Pressure[i,j-2]
            # Interpolating the velocities at the faces
            u_e=linear_interp(u_C,dx_c,u_E,dx_e)
            u_w=linear_interp(u_W,dx_w,u_C,dx_c)
            u_s=linear_interp(u_S,dy_s,u_C,dy_c)
            u_n=linear_interp(u_C,dy_c,u_N,dy_n)
            
            v_e=linear_interp(v_C,dx_c,v_E,dx_e)
            v_w=linear_interp(v_W,dx_w,v_C,dx_c)
            v_s=linear_interp(v_S,dy_s,v_C,dy_c)
            v_n=linear_interp(v_C,dy_c,v_N,dy_n)
            
            # Interpolating the pressure at the faces
            p_e=linear_interp(p_C,dx_c,p_E,dx_e)
            p_w=linear_interp(p_W,dx_w,p_C,dx_c)
            p_s=linear_interp(p_S,dy_s,p_C,dy_c)
            p_n=linear_interp(p_C,dy_c,p_N,dy_n)
            
            #Interpolating the pressure at faces to be used for rhie chow
            p_ee=linear_interp(p_E,dx_e,p_EE,dx_ee)
            p_ww=linear_interp(p_WW,dx_ww,p_w,dx_w)
            p_nn=linear_interp(p_N,dy_n,p_NN,dy_nn)
            p_ss=linear_interp(p_SS,dy_ss,p_S,dy_s)
            
            # Finding the pressure gradients at the cell centres
            dpdx_C=(p_e-p_w)/(dx_c)
            dpdx_E=(p_ee-p_e)/(dx_e)
            dpdx_W=(p_w-p_ww)/(dx_w)
            dpdy_C=(p_n-p_s)/(dy_c)
            dpdy_N=(p_nn-p_n)/(dy_n)
            dpdy_S=(p_s-p_ss)/(dy_s)
            
            factor=timestep/rho
            
            # Finding the pressure gradients at the cell faces
            dpdx_e=(p_E-p_C)/(0.5*(dx_c+dx_e))
            dpdx_w=(p_C-p_w)/(0.5*(dx_w+dx_c))
            dpdy_n=(p_N-p_C)/(0.5*(dy_n+dy_c))
            dpdy_s=(p_C-p_S)/(0.5*(dy_c+dy_s))

            # Coefficients of pressure to be used for rhie and chow interpolation
            coeff_e=factor*(dpdx_e-linear_interp(dpdx_C,dx_c,dpdx_E,dx_e))
            coeff_w=factor*(dpdx_w-linear_interp(dpdx_W,dx_w,dpdx_C,dx_c))
            coeff_n=factor*(dpdy_n-linear_interp(dpdy_C,dy_c,dpdy_N,dy_n))
            coeff_s=factor*(dpdy_s-linear_interp(dpdy_S,dy_s,dpdy_C,dy_c))
            
            # Writing the derivative of pressure using the continuity equation 
            div_dudx=((u_e-coeff_e)-(u_w-coeff_w))/(dx_c)
            div_dvdy=((v_n-coeff_n)-(v_s-coeff_s))/(dy_c)
            
            der_p[i,j]=(-1.0*beta)*(div_dudx+div_dvdy)
            
            # Writing the derivative of u vel using the x momentum equation 
            c_xx=(u_e**2-u_w**2)/(2.0*dx_c)
            c_xy=((u_n*v_n)-(u_s*v_s))*(dy_c)
            
            du_dx_e=(u_E-u_C)/(0.5*(dx_c+dx_e))
            du_dx_w=(u_C-u_W)/(0.5*(dx_w+dx_c))
            
            Du_xx=nu*(du_dx_e-du_dx_w)/dx_c
            
            du_dy_n=(u_N-u_C)/(0.5*(dy_c+dy_n))
            du_dy_s=(u_C-u_S)/(0.5*(dy_s+dy_c))
            
            Du_xy=nu*(du_dy_n-du_dy_s)/dy_c
            
            dp_dx=(1/rho)*(p_e-p_w)/(dx_c)
            
            der_u[i,j]=Du_xx+Du_xy-c_xx-c_xy-dp_dx
            
            # Writing the derivative of v vel using the y momentum equation
            c_yx=(u_e*v_e-u_w*v_w)/dx_c
            c_yy=(v_n**2-v_s**2)/(2*dy_c)
            
            dv_dx_e=(v_E-v_C)/(0.5*(dx_c+dx_e))
            dv_dx_w=(v_C-v_W)/(0.5*(dx_w+dx_c))
            
            Dv_xx=nu*(dv_dx_e-dv_dx_w)/dx_c
            
            dv_dy_n=(v_N-v_C)/(0.5*(dy_c+dy_n))
            dv_dy_s=(v_C-v_S)/(0.5*(dy_s+dy_c))
            
            Dv_xy=nu*(dv_dy_n-dv_dy_s)/dy_c
            
            dp_dy=(1/rho)*(p_n-p_s)/(dy_c)
            
            der_v[i,j]=Dv_xx+Dv_xy-c_yx-c_yy-dp_dy
            
               
            
            
    return der_p,der_u,der_u

@jit(nopython=True,fastmath=True)
def compute_state_der_ibm(u_vel,v_vel,n_neigh,n_lag,lag_i,lag_j,weights_vel,weights_force,timestep):
    # This will compute the lagrangian force given the body is fixed
    der_u=np.zeros_like(u_vel)
    der_v=np.zeros_like(v_vel)
    # Finding out the value of time derivatives of the field subjected to ibm forcing
    for ind in range(n_lag):
        # First the computation of the lagarangian velocity
        u_lag=0.0
        v_lag=0.0
        for n_ind in range(n_neigh):
            i=lag_i[ind,n_ind]
            j=lag_j[ind,n_ind]
            u_lag+=weights_vel[ind,n_ind]*u_vel[i,j]
            v_lag+=weights_vel[ind,n_ind]*v_vel[i,j]
        # Computing the lagrangian force given the velocity
        f_x=(-1.0*u_lag/timestep)
        f_y=(-1.0*v_lag/timestep)
        
        # Now forcing is dispersed back into the eulerian domain
        for n_ind in range(n_neigh):
            i=lag_i[ind,n_ind]
            j=lag_j[ind,n_ind]
            der_u[i,j]+=f_x*weights_force[ind,n_ind]
            der_v[i,j]+=f_y*weights_force[ind,n_ind]
        
    return der_u,der_v

def apply_bc(state,u_in,P_out,m,n):
    # it is assumed that there are two layers of ghost cells
    # m and n are the indicies in the x domain and the y domain respectively
    (Pressure,u_vel,v_vel)=state
    # Applying the boundary conditions at inlet 1
    # First the neumann pressure boundary conditons
    Pressure[0,:]=Pressure[2,:]
    Pressure[1,:]=Pressure[2,:]
    # Second the dirichlet inlet boundary conditons
    u_vel[1,:]=(2*u_in)-u_vel[2,:]
    v_vel[1,:]=(-1.0)*v_vel[2,:]
    
    #Applying the boundary conditions at outlet 2
    # First the dirichlet pressure boundary conditons
    Pressure[m-1,:]=P_out
    Pressure[m-2,:]=P_out
    # Second the neumann velocity boundary condition
    u_vel[m-2,:]=u_vel[m-3,:]
    v_vel[m-2,:]=v_vel[m-3,:]
    
    #Applying the boundary conditions of wall 1
    # First the neumann pressure boundary conditions
    Pressure[:,0]=Pressure[:,2]
    Pressure[:,1]=Pressure[:,2]
    # Second the dirichlet velocity boundary conditions
    u_vel[:,1]=(-1.0)*u_vel[:,2]
    v_vel[:,1]=(-1.0)*v_vel[:,2]
    
    #Applying the boundary conditons of wall 2
    # First the neumann pressure boundary conditions
    Pressure[:,n-1]=Pressure[:,n-3]
    Pressure[:,n-2]=Pressure[:,n-3]
    # Second the dirichlet velocity boundary conditons
    u_vel[:,n-2]=(-1.0)*u_vel[:,n-3]
    v_vel[:,n-2]=(-1.0)*v_vel[:,n-3]
    
    
    return (np.copy(Pressure),np.copy(u_vel),np.copy(v_vel))

    
    
def RK4_update(state,timestep,args_bc,args_der,args_ibm):
    [dx,dy,beta,rho,nu]=args_der
    m=len(dx)
    n=len(dy)
    [u_in,P_out]=args_bc
    [n_neigh,n_lag,lag_i,lag_j,weights_vel,weights_force]=args_ibm
    # Writing all the steps for the time evolution
    
    (P1,U1,V1)=apply_bc(state,u_in,P_out,m,n)
    (d_cfd_p,d_cfd_u,d_cfd_v)=compute_state_der_cfd(P1,U1,V1,dx,dy,beta,rho,nu,timestep)
    (d_ibm_u,d_ibm_v)=compute_state_der_ibm(U1,V1,n_neigh,n_lag,lag_i,lag_j,weights_vel,weights_force,timestep)
    dP1=np.copy(d_cfd_p)
    dU1=np.copy(d_cfd_u+d_ibm_u)
    dV1=np.copy(d_cfd_v+d_ibm_v)
    
    state2=(P1+0.5*timestep*dP1,U1+0.5*timestep*dU1,V1+0.5*timestep*dV1)
    
    (P2,U2,V2)=apply_bc(state2,u_in,P_out,m,n)
    (d_cfd_p,d_cfd_u,d_cfd_v)=compute_state_der_cfd(P2,U2,V2,dx,dy,beta,rho,nu,timestep)
    (d_ibm_u,d_ibm_v)=compute_state_der_ibm(U2,V2,n_neigh,n_lag,lag_i,lag_j,weights_vel,weights_force,timestep)
    dP2=np.copy(d_cfd_p)
    dU2=np.copy(d_cfd_u+d_ibm_u)
    dV2=np.copy(d_cfd_v+d_ibm_v)
    
    state3=(P1+0.5*timestep*dP2,U1+0.5*timestep*dU2,V1+0.5*timestep*dV2)
    
    (P3,U3,V3)=apply_bc(state3,u_in,P_out,m,n)
    (d_cfd_p,d_cfd_u,d_cfd_v)=compute_state_der_cfd(P3,U3,V3,dx,dy,beta,rho,nu,timestep)
    (d_ibm_u,d_ibm_v)=compute_state_der_ibm(U3,V3,n_neigh,n_lag,lag_i,lag_j,weights_vel,weights_force,timestep)
    dP3=np.copy(d_cfd_p)
    dU3=np.copy(d_cfd_u+d_ibm_u)
    dV3=np.copy(d_cfd_v+d_ibm_v)
    
    state4=(P1+timestep*dP3,U1+timestep*dU3,V1+timestep*dV3)
    
    (P4,U4,V4)=apply_bc(state4,u_in,P_out,m,n)
    (d_cfd_p,d_cfd_u,d_cfd_v)=compute_state_der_cfd(P4,U4,V4,dx,dy,beta,rho,nu,timestep)
    (d_ibm_u,d_ibm_v)=compute_state_der_ibm(U4,V4,n_neigh,n_lag,lag_i,lag_j,weights_vel,weights_force,timestep)
    dP4=np.copy(d_cfd_p)
    dU4=np.copy(d_cfd_u+d_ibm_u)
    dV4=np.copy(d_cfd_v+d_ibm_v)
    
    # Now taking weighted sum of derivatives
    dP=timestep*(1/6.0)*(dP1+dP4+0.5*(dP2+dP3))
    dU=timestep*(1/6.0)*(dU1+dU4+0.5*(dU2+dU3))
    dV=timestep*(1/6.0)*(dV1+dV4+0.5*(dV2+dV3))
    
    return (np.copy(P1+dP),np.copy(U1+dU),np.copy(V1+dV))


    
@jit(nopython=True)   
def calculate_interpolation_weights_indices(x_grid,y_grid,lag_x,lag_y,n_neigh):
    # n_neigh are the number of neighbours that the point will have in one direction
    points_neigh=(2*n_neigh)**2
    weights_vel=np.zeros([len(lag_x),points_neigh])
    weights_force=np.zeros([len(lag_x),points_neigh])
    i_ind=np.zeros([len(lag_x),points_neigh],dtype=np.int64)
    j_ind=np.zeros([len(lag_x),points_neigh],dtype=np.int64)
    
    # It is assumed that the lagrangian points form a closed curve
    # Thus P0-P1-P2.....P(N-1)-P0 is the curve in question 
    # This is done to compute the arc lengths that will be essesntial for weight calculations
    
    for ind in range(len(lag_x)):
        x_cord=lag_x[ind] # x coordinate of the lagrangian point
        y_cord=lag_y[ind] # y coordinate of the lagrangian point
        # Now let us find the nearest indices of the points
        near_i=0
        near_j=0
        for i in range(len(x_grid)-1):
            if x_cord>=x_grid[i] and x_cord<x_grid[i+1]:
                near_i=i+0
                break
        
        for j in range(len(y_grid)-1):
            if y_cord>=y_grid[j] and y_cord<y_grid[j+1]:
                near_j=j+0
                break
        # Now we need to precompute some quatitites and then
        # Calculate the neighbours and the indices 
        dx=x_grid[near_i+1]-x_grid[near_i]
        dy=y_grid[near_j+1]-y_grid[near_j]
        dh=(dx*dy)**0.5 # Thickness of the immersed boundary
        # Now we need to find the length of the immersed boundary element
        fwd=ind+1
        back=ind-1
        if ind==0:
            back=len(lag_x)-1
        if ind==(len(lag_x)-1):
            fwd=0
        df=((lag_x[fwd]-lag_x[ind])**2+(lag_y[fwd]-lag_y[ind])**2)**0.5
        db=((lag_x[back]-lag_x[ind])**2+(lag_y[back]-lag_y[ind])**2)**0.5
        ds=0.5*(df+db)
        # Now we are ready to find out the interpolation weights
        curr=0
        for i in range(near_i-n_neigh+1,near_i+n_neigh+1):
            for j in range(near_j-n_neigh+1,near_j+n_neigh+1):
                x_eu=x_grid[i] # X Coordinate of the euler point
                y_eu=y_grid[j] # Y Coordinate of the euler point
                rx=(abs(x_eu-x_cord))/dx
                ry=(abs(y_eu-y_cord))/dy
                delta_x=0.0
                delta_y=0.0
                if rx<=2.0:
                    delta_x=0.25*(1+np.cos(np.pi*0.5*rx))
                if ry<=2.0:
                    delta_y=0.25*(1+np.cos(np.pi*0.5*ry))
                delta=delta_x*delta_y
                i_ind[ind,curr]=i+0
                j_ind[ind,curr]=j+0
                weights_vel[ind,curr]=delta+0.0
                weights_force[ind,curr]=delta*(ds*dh)/(dx*dy)
                curr=curr+1
            
    return points_neigh,weights_vel,weights_force,i_ind,j_ind
 
# It is assumed that there are two layers of ghost cells on either side 
def create_euler_domain(x,y,U_init,P_out):
    # This function will return the cell centres,step lengths and initial states 
    dx_b=x[1]-x[0]
    dx_f=x[-1]-x[-2]
    xb=x[0]-dx_b
    xbb=xb-dx_b
    xf=x[-1]+dx_f
    xff=xf+dx_f
    x_arr=np.concatenate([[xbb,xb],x,[xf,xff]])
    
    dy_b=y[1]-y[0]
    dy_f=y[-1]-y[-2]
    yb=y[0]-dy_b
    ybb=yb-dy_b
    yf=y[-1]+dy_f
    yff=yf+dy_f
    y_arr=np.concatenate([[ybb,yb],y,[yf,yff]])
    
    x_c=0.5*(x_arr[1:]+x_arr[:-1])
    y_c=0.5*(y_arr[1:]+y_arr[:-1])
    dx=x_arr[1:]-x_arr[:-1]
    dy=y_arr[1:]-y_arr[:-1]
    U=U_init*np.ones([len(x_c),len(y_c)])
    V=np.zeros_like(U)
    P=P_out*np.ones_like(U)
    
    return [x_c,y_c,dx,dy,U,V,P]
    
def generate_1D_cartesian_domains(data):
    # Data is the dictionary defined with the following values
    radius=0.5*data["Dim"]      # Characteristic dimension of the object
    dx=data["dx"]               # Charasteristic spacing in the x direction
    dy=data["dy"]               # Characteristic spacing in the y direction
    n_wall=data["wall_div"]     # No of divisions from concerntration region to wall
    n_inlet=data["inlet_div"]   # No of divisions from concerntration region to inlet
    n_outlet=data["outlet_div"] # No of divisions from concerntration region to outlet
    factor_conc=data["f_conc"]  # Factor that governs the dimension of the concerntration region
    factor_wall=data["f_wall"]  # Factor that governs the dimension of the extent of the wall region
    factor_outlet=data["f_out"] # Factor that governs the dimension of the extent of the outlet region
    factor_inlet=data["f_in"]   # Factor that governs the dimension of the extent of the inlet region 
    
    x_mid=np.arange(-1*factor_conc*radius,factor_conc*radius,dx)
    x_pre=np.linspace(-1*factor_inlet*radius,-1*factor_conc*radius,n_inlet)
    x_pos=np.linspace(factor_conc*radius,factor_outlet*radius,n_outlet)

    y_mid=np.arange(-1*factor_conc*radius,1*factor_conc*radius,dy)
    y_pre=np.linspace(-1*factor_wall*radius,-1*factor_conc*radius,n_wall)
    y_pos=np.linspace(factor_conc*radius,factor_wall*radius,n_wall)

    x_array=np.concatenate([x_pre[:-1],x_mid,x_pos])
    y_array=np.concatenate([y_pre[:-1],y_mid,y_pos])
    
    return x_array,y_array


def post_process_data(state_space,x_c,y_c):
    xx,yy=np.meshgrid(x_c,y_c,indexing="ij")
    P_state=[]
    U_state=[]
    V_state=[]
    for i in range(len(state_space)):
        (P_curr,U_curr,V_curr)=state_space[i]
        P_state.append(np.copy(P_curr))
        U_state.append(np.copy(U_curr))
        V_state.append(np.copy(V_curr))
        
    return xx,yy,P_state,U_state,V_state

def find_min_size(x,y):
    x_mod=np.concatenate([[x[-1]],x,[x[0]]])
    y_mod=np.concatenate([[y[-1]],y,[y[0]]])
    x_f=x_mod[2:]
    x_b=x_mod[:-2]
    x_c=x_mod[1:-1]
    y_f=y_mod[2:]
    y_b=y_mod[:-2]
    y_c=y_mod[1:-1]
    ds_f=((x_f-x_c)**2+(y_f-y_c)**2)**0.5
    ds_b=((x_f-x_c)**2+(y_f-y_c)**2)**0.5
    return np.min(0.5*(ds_f+ds_b))

