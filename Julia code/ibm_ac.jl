module ibm_ac

using Flux;
using Zygote;

@inline function linear_interp(var1,dx1,var2,dx2)
    return ((dx2/(dx1+dx2))*var1)+((dx1/(dx1+dx2))*var2)
end

function AC_solver_state(Pressure,u_vel,v_vel,dx,dy,beta,rho,nu,timestep)
    # This will give the temporal derivative for the CFD state variables 
    # First let us generate the Zygote buffer arrays to be used by differentiable solver
    der_p=Zygote.Buffer(Pressure,size(Pressure)[1],size(Pressure)[2]);
    der_u=Zygote.Buffer(u_vel,size(u_vel)[1],size(u_vel)[2]);
    der_v=Zygote.Buffer(v_vel,size(v_vel)[1],size(v_vel)[2]);
    # Now let us loop over all the elements of the grid to generate the time derivatives
    for i=3:(size(Pressure)[1]-2),j=3:(size(Pressure)[2]-2)
        # Writing the variables to be used for writing the equations
        # Capital denotes the centre velocities
        dx_c=dx[i];
        dx_e=dx[i+1];
        dx_ee=dx[i+2];
        dx_w=dx[i-1];
        dx_ww=dx[i-2];
        
        dy_c=dy[j];
        dy_n=dy[j+1];
        dy_nn=dy[j+2];
        dy_s=dy[j-1];
        dy_ss=dy[j-2];
        
        u_C=u_vel[i,j];
        u_E=u_vel[i+1,j];
        u_W=u_vel[i-1,j];
        u_N=u_vel[i,j+1];
        u_S=u_vel[i,j-1];
        
        v_C=v_vel[i,j];
        v_E=v_vel[i+1,j];
        v_W=v_vel[i-1,j];
        v_N=v_vel[i,j+1];
        v_S=v_vel[i,j-1];

        p_C=Pressure[i,j];
        p_E=Pressure[i+1,j];
        p_EE=Pressure[i+2,j];
        p_W=Pressure[i-1,j];
        p_WW=Pressure[i-2,j];
        p_N=Pressure[i,j+1];
        p_NN=Pressure[i,j+2];
        p_S=Pressure[i,j-1];
        p_SS=Pressure[i,j-2];
        # Interpolating the velocities at the faces
        u_e=linear_interp(u_C,dx_c,u_E,dx_e);
        u_w=linear_interp(u_W,dx_w,u_C,dx_c);
        u_s=linear_interp(u_S,dy_s,u_C,dy_c);
        u_n=linear_interp(u_C,dy_c,u_N,dy_n);

        v_e=linear_interp(v_C,dx_c,v_E,dx_e);
        v_w=linear_interp(v_W,dx_w,v_C,dx_c);
        v_s=linear_interp(v_S,dy_s,v_C,dy_c);
        v_n=linear_interp(v_C,dy_c,v_N,dy_n);
        
        # Interpolating the pressure at the faces
        p_e=linear_interp(p_C,dx_c,p_E,dx_e);
        p_w=linear_interp(p_W,dx_w,p_C,dx_c);
        p_s=linear_interp(p_S,dy_s,p_C,dy_c);
        p_n=linear_interp(p_C,dy_c,p_N,dy_n);
        #Interpolating the pressure at faces to be used for rhie chow
        p_ee=linear_interp(p_E,dx_e,p_EE,dx_ee);
        p_ww=linear_interp(p_WW,dx_ww,p_w,dx_w);
        p_nn=linear_interp(p_N,dy_n,p_NN,dy_nn);
        p_ss=linear_interp(p_SS,dy_ss,p_S,dy_s);
        
        # Finding the pressure gradients at the cell centres
        dpdx_C=(p_e-p_w)/(dx_c);
        dpdx_E=(p_ee-p_e)/(dx_e);
        dpdx_W=(p_w-p_ww)/(dx_w);
        dpdy_C=(p_n-p_s)/(dy_c);
        dpdy_N=(p_nn-p_n)/(dy_n);
        dpdy_S=(p_s-p_ss)/(dy_s);
        
        factor=timestep/rho;
        
        # Finding the pressure gradients at the cell faces
        dpdx_e=(p_E-p_C)/(0.5*(dx_c+dx_e));
        dpdx_w=(p_C-p_w)/(0.5*(dx_w+dx_c));
        dpdy_n=(p_N-p_C)/(0.5*(dy_n+dy_c));
        dpdy_s=(p_C-p_S)/(0.5*(dy_c+dy_s));

       # Coefficients of pressure to be used for rhie and chow interpolation
        coeff_e=factor*(dpdx_e-linear_interp(dpdx_C,dx_c,dpdx_E,dx_e));
        coeff_w=factor*(dpdx_w-linear_interp(dpdx_W,dx_w,dpdx_C,dx_c));
        coeff_n=factor*(dpdy_n-linear_interp(dpdy_C,dy_c,dpdy_N,dy_n));
        coeff_s=factor*(dpdy_s-linear_interp(dpdy_S,dy_s,dpdy_C,dy_c));
        
        # Writing the derivative of pressure using the continuity equation 
        div_dudx=((u_e-coeff_e)-(u_w-coeff_w))/(dx_c);
        div_dvdy=((v_n-coeff_n)-(v_s-coeff_s))/(dy_c);
        
        der_p[i,j]=(-1.0*beta)*(div_dudx+div_dvdy);
        
        # Writing the derivative of u vel using the x momentum equation 
        c_xx=(u_e^2-u_w^2)/(2.0*dx_c);
        c_xy=((u_n*v_n)-(u_s*v_s))*(dy_c);
        
        du_dx_e=(u_E-u_C)/(0.5*(dx_c+dx_e));
        du_dx_w=(u_C-u_W)/(0.5*(dx_w+dx_c));
        
        Du_xx=nu*(du_dx_e-du_dx_w)/dx_c;
        
        du_dy_n=(u_N-u_C)/(0.5*(dy_c+dy_n));
        du_dy_s=(u_C-u_S)/(0.5*(dy_s+dy_c));
        
        Du_xy=nu*(du_dy_n-du_dy_s)/dy_c;
        
        dp_dx=(1/rho)*(p_e-p_w)/(dx_c);
        
        der_u[i,j]=Du_xx+Du_xy-c_xx-c_xy-dp_dx;
        
        # Writing the derivative of v vel using the y momentum equation
        c_yx=(u_e*v_e-u_w*v_w)/dx_c;
        c_yy=(v_n^2-v_s^2)/(2*dy_c);
        
        dv_dx_e=(v_E-v_C)/(0.5*(dx_c+dx_e));
        dv_dx_w=(v_C-v_W)/(0.5*(dx_w+dx_c));
        
        Dv_xx=nu*(dv_dx_e-dv_dx_w)/dx_c;
        
        dv_dy_n=(v_N-v_C)/(0.5*(dy_c+dy_n));
        dv_dy_s=(v_C-v_S)/(0.5*(dy_s+dy_c));
        
        Dv_xy=nu*(dv_dy_n-dv_dy_s)/dy_c;
        
        dp_dy=(1/rho)*(p_n-p_s)/(dy_c);
        
        der_v[i,j]=Dv_xx+Dv_xy-c_yx-c_yy-dp_dy;
    end
    return copy(der_p),copy(der_u),copy(der_v);
end

function CF_IBM(u_vel,v_vel,n_neigh,n_lag,lag_i,lag_j,weights_vel,weights_force,timestep)
    # First generating the buffer arrays to take data in account
    der_u=Zygote.Buffer(u_vel,size(u_vel)[1],size(u_vel)[2]);
    der_v=Zygote.Buffer(v_vel,size(v_vel)[1],size(v_vel)[2]);
    # Now loop over all lagrange points
    for ind=1:n_lag
        u_lag=0.0;
        v_lag=0.0;
        for n_ind=1:n_neigh
            i=lag_i[ind,n_ind];
            j=lag_j[ind,n_ind];
            u_lag+=weights_vel[ind,n_ind]*u_vel[i,j];
            v_lag+=weights_vel[ind,n_ind]*v_vel[i,j];
        end
        # Computing the lagrangian force given the velocity
        f_x=(-1.0*u_lag/timestep);
        f_y=(-1.0*v_lag/timestep);

        # Now forcing is dispersed back into the eulerian domain
        for n_ind=1:n_neigh
            i=lag_i[ind,n_ind];
            j=lag_j[ind,n_ind];
            der_u[i,j]+=f_x*weights_force[ind,n_ind];
            der_v[i,j]+=f_y*weights_force[ind,n_ind];
        end
    end
    return copy(der_u),copy(der_v);
end

function apply_bc(Pressure,u_vel,v_vel,u_in,P_out)
    # Apply the inlet and the outlet boundary conditions for inlet and outlet
    # Apply the wall boundary conditions at the two walls
    m=size(Pressure)[1];
    n=size(Pressure)[2];
    Pressure_bc=Zygote.Buffer(Pressure,m,n);
    u_vel_bc=Zygote.Buffer(u_vel,m,n);
    v_vel_bc=Zygote.Buffer(v_vel,m,n);

    # Apply the inlet bc 
    # First the neumann pressure bc
    Pressure_bc[1,:]=Pressure[3,:];
    Pressure_bc[2,:]=Pressure[3,:];
    # Now the dirichlet velocity bc
    u_vel_bc[2,:]=2.0*u_in.-u_vel[3,:];
    u_vel_bc[1,:]=u_vel_bc[2,:];
    v_vel_bc[2,:]=(-1.0).*v_vel[3,:];
    v_vel_bc[1,:]=v_vel_bc[2,:];

    # Apply the outlet bc
    # First the dirichlet pressure bc
    Pressure_bc[m,:].=P_out;
    Pressure_bc[m-1,:].=P_out;
    # Now the neumann velocity bc
    u_vel_bc[m-1,:]=u_vel[m-2,:];
    u_vel_bc[m,:]=u_vel_bc[m-1,:];
    v_vel_bc[m-1,:]=v_vel[m-2,:];
    v_vel_bc[m,:]=v_vel_bc[m-1,:];
    
    # Apply wall 1 bc
    # First the neumann pressure bc
    Pressure_bc[:,2]=Pressure[:,3];
    Pressure_bc[:,1]=Pressure_bc[:,2];

    # Now the dirichlet walls bc
    u_vel_bc[:,2]=(-1.0).*u_vel[:,3];
    u_vel_bc[:,1]=u_vel_bc[:,2];
    v_vel_bc[:,2]=(-1.0).*v_vel[:,3];
    v_vel_bc[:,1]=v_vel_bc[:,2];
    
    
    # Apply wall 2 bc
    # First the neumann pressure bc
    Pressure_bc[:,n-1]=Pressure[:,n-2];
    Pressure_bc[:,n]=Pressure_bc[:,n-1];

    # Now the dirichlet walls bc
    u_vel_bc[:,n-1]=(-1.0).*u_vel[:,n-2];
    u_vel_bc[:,n]=u_vel_bc[:,n-1];
    v_vel_bc[:,n-1]=(-1.0).*v_vel[:,n-2];
    v_vel_bc[:,n]=v_vel_bc[:,n-1];


    # Copy the rest of the values
    Pressure_bc[3:(m-2),3:(n-2)]=Pressure[3:(m-2),3:(n-2)];
    u_vel_bc[3:(m-2),3:(n-2)]=u_vel[3:(m-2),3:(n-2)];
    v_vel_bc[3:(m-2),3:(n-2)]=v_vel[3:(m-2),3:(n-2)];

    return copy(Pressure_bc),copy(u_vel_bc),copy(v_vel_bc);

end

function RK4_Simulation(P_state,U_state,V_state,timestep,
                        dx,dy,beta,rho,nu, # Args of solver
                        U_in,P_out,        # Boundary Condtions
                        n_neigh,n_lag,lag_i,lag_j,weights_vel,weights_force) # Immersed boundary method functions
    
    m,n=size(P_state);
    AC_dP1,AC_dU1,AC_dV1=AC_solver_state(P_state,U_state,V_state,dx,dy,beta,rho,nu,timestep);
    IB_dU1,IB_dV1=CF_IBM(U_state,V_state,n_neigh,n_lag,lag_i,lag_j,weights_vel,weights_force,timestep);
    
    P_nb2=P_state+0.5*timestep*AC_dP1;
    U_nb2=U_state+0.5*timestep*(AC_dU1+IB_dU1);
    V_nb2=V_state+0.5*timestep*(AC_dV1+IB_dV1);

    P2,U2,V2=apply_bc(P_nb2,U_nb2,V_nb2,U_in,P_out);

    AC_dP2,AC_dU2,AC_dV2=AC_solver_state(P2,U2,V2,dx,dy,beta,rho,nu,timestep);
    IB_dU2,IB_dV2=CF_IBM(U2,V2,n_neigh,n_lag,lag_i,lag_j,weights_vel,weights_force,timestep);
    
    P_nb3=P_state+0.5*timestep*AC_dP2;
    U_nb3=U_state+0.5*timestep*(AC_dU2+IB_dU2);
    V_nb3=V_state+0.5*timestep*(AC_dV2+IB_dV2);

    P3,U3,V3=apply_bc(P_nb3,U_nb3,V_nb3,U_in,P_out);

    AC_dP3,AC_dU3,AC_dV3=AC_solver_state(P3,U3,V3,dx,dy,beta,rho,nu,timestep);
    IB_dU3,IB_dV3=CF_IBM(U3,V3,n_neigh,n_lag,lag_i,lag_j,weights_vel,weights_force,timestep);
        
    P_nb4=P_state+1.0*timestep*AC_dP3;
    U_nb4=U_state+1.0*timestep*(AC_dU3+IB_dU3);
    V_nb4=V_state+1.0*timestep*(AC_dV3+IB_dV3);

    P4,U4,V4=apply_bc(P_nb4,U_nb4,V_nb4,U_in,P_out);

    AC_dP4,AC_dU4,AC_dV4=AC_solver_state(P4,U4,V4,dx,dy,beta,rho,nu,timestep);
    IB_dU4,IB_dV4=CF_IBM(U4,V4,n_neigh,n_lag,lag_i,lag_j,weights_vel,weights_force,timestep);
    
    dPe=(AC_dP1+2.0*(AC_dP2+AC_dP3)+AC_dP4)/6.0;
    dUe=(AC_dU1+2.0*(AC_dU2+AC_dU3)+AC_dU4)/6.0+(IB_dU1+2.0*(IB_dU2+IB_dU3)+IB_dU4)/6.0;
    dVe=(AC_dV1+2.0*(AC_dV2+AC_dV3)+AC_dV4)/6.0+(IB_dV1+2.0*(IB_dV2+IB_dV3)+IB_dV4)/6.0;

    P_eff=P_state+dPe;
    U_eff=U_state+dUe;
    V_eff=V_state+dVe;

    new_P,new_U,new_V=apply_bc(P_eff,U_eff,V_eff,U_in,P_out);

    return new_P,new_U,new_V;
end

function Initialize_domain(x_grid,y_grid,U_in,P_out)
    # Used for initializing the domain using constant values
    m=length(x_grid);
    n=length(y_grid);
    U_vel=U_in*ones(m,n);
    V_vel=zeros(m,n);
    Pressure=P_out*ones(m,n);

    return Pressure,U_vel,V_vel;
end

function Generate_interpolation_weights(x_grid,y_grid,lag_x,lag_y,n_neigh)
    # used for computing interpolation for lagrangian velocity anf forces
    points_neigh=(2*n_neigh)^2;
    lag_len=length(lag_x);
    # Defining the weights for interpolation of velocity to lagrangian points
    weights_vel=Zygote.Buffer(zeros(Float64,lag_len,points_neigh),lag_len,points_neigh);    
    # Defining the weights for spreading the force in the eulerian domain
    weights_force=Zygote.Buffer(zeros(Float64,lag_len,points_neigh),lag_len,points_neigh);
    # Defining the weights for the compuatation of the net force
    weights_c_f=Zygote.Buffer(zeros(Float64,lag_len,points_neigh),lag_len,points_neigh);
    # Now defining the neighbour eulerian indices for each lagrangian points
    i_ind=Zygote.Buffer(zeros(Int64,lag_len,points_neigh),lag_len,points_neigh);
    j_ind=Zygote.Buffer(zeros(Int64,lag_len,points_neigh),lag_len,points_neigh);

    # Now iterating over each lagrangian points
    for ind=1:lag_len
        x_cord=lag_x[ind]; # X coordinate of the lagrangian point
        y_cord=lag_y[ind]; # Y coordinate of the lagrangian point
        # Now find the nearest neighbours
        near_i=0;
        near_j=0;
        for i=1:(length(x_grid)-1)
            if (x_cord>=x_grid[i])&&(x_cord<x_grid[i+1])
                near_i=i+0;
                break
            end
        end
        for j=1:(length(y_grid)-1)
            if (y_cord>=y_grid[j])&&(y_cord<y_grid[j+1])
                near_j=j+0;
                break
            end
        end
        # Now we precompute some quantities and then find neighbour indices
        dx=x_grid[near_i+1]-x_grid[near_i];
        dy=y_grid[near_j+1]-y_grid[near_j];
        dh=(dx*dy)^0.5;
        # We assume the width of the immersed boundary is same as the body
        ds=dh+0.0;
        # Now we are ready to find the interpolation weights
        curr=1;
        for i=(near_i-n_neigh+1):(near_i+n_neigh),j=(near_j-n_neigh+1):(near_j+n_neigh)
            x_eu=x_grid[i]; # x coordinate of the eulerian cell
            y_eu=y_grid[j]; # y coordinate of the eulerian cell
            rx=abs(x_eu-x_cord)/dx;
            ry=abs(y_eu-y_cord)/dy;
            delta_x=0.0;
            delta_y=0.0;
            if rx<=2.0
                delta_x=0.25*(1+cos(0.5*pi*rx));
            end
            if ry<=2.0
                delta_y=0.25*(1+cos(0.5*pi*ry));
            end
            delta_val=delta_x*delta_y;
            i_ind[ind,curr]=i+0;
            j_ind[ind,curr]=j+0;
            weights_vel[ind,curr]=delta_val+0.0;
            weights_force[ind,curr]=delta_val*(ds*dh)/(dx*dy);
            weights_c_f[ind,curr]=delta_val*(ds*dh);
            curr=curr+1;
        end
    end

    return points_neigh,weights_vel,weights_force,weights_c_f,i_ind,j_ind;

end

function Compute_force(u_vel,v_vel,n_neigh,n_lag,lag_i,lag_j,weights_c_f,rho,timestep)
    # Compute the drag and the lift forces on the body
    F_x=0.0; #initialize the force in the x direction 
    F_y=0.0; #initialize the force in the y direction
    # Now loop over all lagrangian points
    for ind=1:n_lag
        u_lag=0.0;
        v_lag=0.0;
        for n_ind=1:n_neigh
            i=lag_i[ind,n_ind];
            j=lag_j[ind,n_ind];
            u_lag+=weights_c_f[ind,n_ind]*u_vel[i,j];
            v_lag+=weights_c_f[ind,n_ind]*v_vel[i,j];
        end
        # Computing the lagrangian force given the velocity
        F_x=F_x+(-1.0*rho*u_lag/timestep);
        F_y=F_y+(-1.0*rho*v_lag/timestep);

    end
    return F_x,F_y;
end

export Generate_interpolation_weights,Initialize_domain,RK4_Simulation,Compute_force;
end