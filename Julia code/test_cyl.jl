# Writing the simulation for cylinder geometry
include("ibm_ac.jl");
using Flux;
using Zygote;
using HDF5; 

# Defining the circle geometry
theta=0:0.01:2*pi;
radius=0.5;
x=radius*cos.(theta[1:end-1])
y=radius*sin.(theta[1:end-1])
n_lag=size(x)[1];

# Now let us define the grid
x_grid_vals=-7*radius:(radius/60.0):10.0*radius
y_grid_vals=-7*radius:(radius/40.0):7*radius
x_c=0.5*(x_grid_vals[1:end-1]+x_grid_vals[2:end])
y_c=0.5*(y_grid_vals[1:end-1]+y_grid_vals[2:end])
dx=(x_grid_vals[2:end]-x_grid_vals[1:end-1])
dy=(y_grid_vals[2:end]-y_grid_vals[1:end-1])

# Now let us define the fluid parameters
L=2*radius;
nu=1e-2;
rho=1.0;
Re=200;
U_init=nu*Re/L
P_out=0.0;

# Now let us define the simulation parameters
timestep=1e-6;
beta=0.1;


points_neigh,weights_vel,weights_force,weights_c_f,i_ind,j_ind=ibm_ac.Generate_interpolation_weights(x_c,y_c,x,y,3);
curr_P,curr_u,curr_v=ibm_ac.Initialize_domain(x_c,y_c,U_init,P_out);

curr_P,curr_u,curr_v=ibm_ac.RK4_Simulation(curr_P,curr_u,curr_v,timestep,dx,dy,beta,rho,nu,U_init,P_out,points_neigh,n_lag,i_ind,j_ind,weights_vel,weights_force);
ibm_ac.Compute_force(curr_u,curr_v,points_neigh,n_lag,i_ind,j_ind,weights_c_f,rho,timestep)